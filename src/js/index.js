/*jslint sub: true*/
var batt = require('./battery.js');
Pebble.addEventListener('ready', function(e) {
  console.log('PebbleKit JS ready!');
  batt.Battery_Init();
  batt.Battery_GetData();
});
