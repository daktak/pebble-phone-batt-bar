#pragma once
#include <pebble.h>

typedef Layer PhoneBattBarLayer;

PhoneBattBarLayer* phone_batt_bar_layer_create();
void phone_batt_bar_layer_destroy(PhoneBattBarLayer *phone_batt_bar_layer);
void phone_batt_bar_set_percent_hidden(bool hidden);
void phone_batt_bar_set_icon_hidden(bool hidden);
void phone_batt_bar_set_colors(GColor normal, GColor warning, GColor danger, GColor charging);
void phone_batt_bar_set_position(GPoint position);
void phone_batt_bar_load(const uint32_t key);
void phone_batt_bar_save(const uint32_t key);
