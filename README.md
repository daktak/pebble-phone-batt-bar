# pebble-phone-batt-bar

Pebble library to add a simple phone battery bar to your watch up

Copied and adapted from https://github.com/orviwan/pebble-battery-bar

## Usage

`pebble package install pebble-phone-batt-bar`

````js
var phone_batt = require('pebble-phone-batt-bar');

````


````c
// This is a simple example, demonstrating the basic usage.
#include <pebble-phone-batt-bar/pebble-phone-batt-bar.h>

static PhoneBattBarLayer *s_battery_layer;

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);

  s_battery_layer = phone_batt_bar_layer_create();
  phone_batt_bar_load(PHONE_KEY);
  layer_add_child(window_layer, s_battery_layer);
}

static void window_unload(Window *window) {
  phone_batt_bar_save(PHONE_KEY);
  phone_batt_bar_layer_destroy(s_battery_layer);
}

````

## Additional Settings

Position the battery bar on screen.

    void phone_batt_bar_set_position(GPoint position);

Hide the percentage text (100%).

    void phone_batt_bar_set_percent_hidden(bool hidden);

Hide the battery icon.

    void phone_batt_bar_set_icon_hidden(bool hidden);

Override the color scheme for each state.

    void phone_batt_bar_set_colors(GColor normal, GColor warning, GColor danger, GColor charging);
